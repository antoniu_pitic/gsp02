#include "Calcule.h"
#include <iostream>
using namespace std;

void CalculF(int a, int b, int c) {
	double f;

	f = (2 * a + b) / float(c);

	cout << "f = (2 * " << a << " + ";
	cout << b << ") / " << c << " = " << f;
	cout << endl;

}
